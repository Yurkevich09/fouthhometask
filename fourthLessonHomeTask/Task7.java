
package fourthLessonHomeTask;

import java.io.IOException;
import java.util.Scanner;

public class Task7 {
    // Калькулятор в консоли
    public static void main(String[] args){
        int number1 = getInt(1);
        char symbol1 = getOperation();
        int number2 = getInt(2);
        int result = calc(number1, symbol1, number2);
        System.out.println(number1 + " " + symbol1 + " " + number2 + " = " + result);
    }

    public static int getInt(int i){
        System.out.print("Введите " + i + "-е число: ");
        Scanner inputNumber = new Scanner(System.in);
        int l = inputNumber.nextInt();
        return l;
    }

    public static char getOperation() {
        System.out.print("Введите математический символ: ");
        Scanner inputSymbol = new Scanner(System.in);
        char s = inputSymbol.next().charAt(0);
        return s;
    }

    public static int calc (int num1, char operation, int num2){
        int result = 0;
        System.out.print("Результат вычисления: ");
        if (operation == '/' && num2 != 0 && num1 != 0){
            result = num1 / num2;
        }
        else if (operation == '*'){
            result = num1 * num2;
        }
        else if (operation == '+'){
            result = num1 + num2;
        }
        else if (operation == '-'){
            result = num1 - num2;
        }
        else {
            System.out.println("Ошибка! Попробуйте еще раз!");
            return 0;
        }
        return result;
    }
}
